# Smoke test 07/20/20

**Objective**: Build an android app from scratch that has two screens and the main goal is to count how many times the same items are appear in the list.

**Code**: It is preferable Kotlin.

## Screen 1

### Contains:

- A list of elements (strings), located on top;
- 3 buttons: send, calculate and clear (less important), located in the bottom;
- A text box, located on top of the buttons.

### Funcitonalities:

- User can insert items on the list, using the button and the text box;
- User can clear the list using the button;
- User can calculate the items using the button.

## Screen 2

### Contains:

- A list of elements (strings), located on top, that contains the calculus result;
- 1 button back, located in the bottom;

## Extras

- The list must be empty in the beggining, but the user must be able to see where the list is;
- The items must have just characters [a-z,A-Z,␣]
- The calculus must not consider case sensitive.
- The app must avoid bad user experiences (crashes, unexpected behaviors, etc).

## Input list example

```
banana
tomato
noodles
noodles
banAna
banaNa
Fresh Beans
Fresh BEANS
```

## Output list example

```
Banana: 3
Fresh Beans: 2
Noodles: 2
Tomato: 1
```

## Hints

- I will evaluate your approach first, later your design;
- You must be able to explain the technologies you will use.
